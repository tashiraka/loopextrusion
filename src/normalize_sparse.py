# coding:utf-8
import numpy as np
import pandas as pd
from scipy import sparse
import sys

'''
# Knight, P. a., & Ruiz, D. (2013). A fast algorithm for matrix balancing. IMA Journal of Numerical Analysis, 33, 1029–1047. doi:10.1093/imanum/drs019

# BNEWT A balancing algorithm for symmetric matrices 
# 
# X = BNEWT(A) attempts to find a vector X such that
# diag(X)*A*diag(X) is close to doubly stochastic. A must 
# be symmetric, nonnegative and upper-triangle. 
# 
# A: supporsed to be CSR format
# X0: initial guess. TOL: error tolerance. 
# delta/Delta: how close/far balancing vectors can get 
# to/from the edge of the positive cone. 
# We use a relative measure on the size of elements. 
# FL: intermediate convergence statistics on/off. 
# RES: residual error, measured by norm(diag(x)*A*x-e).
#
# Returned x is sparse format
#
'''
def bnewt_sparse(A, tol=None, x0=None, delta=None, Delta=None, fl=None):
    # A vector is implemented as dense array.
    
    # Initialize
    n = A.shape[0]
    e = np.ones(n) # dense
    res = []; 
    if fl    is None: fl = 0.0
    if Delta is None: Delta = 3.0
    if delta is None: delta = 0.1 # Default value
    #if delta is None: delta = 0.25 # use if not-convergent case e.g. oscilation
    if x0    is None: x0 = e
    if tol   is None: tol = 1e-6 

    # Inner stopping criterion parameters. 
    g = 0.9
    etamax = 0.1 # Default value
    #etamax = 0.01 # use if not-convergent case e.g. oscilation
    eta = etamax 
    stop_tol = tol * 0.5
    x = x0
    rt = tol ** 2
    v = x * A.dot(x) # x.*(A*x) in matlab
    rk = 1.0 - v # dense
    #rk = sparse(1) - v  # sparse
    rho_km1 = rk.dot(rk)
    rout = rho_km1
    rold = rout
    
    MVP = 0 # We’ll count matrix vector products. 
    i = 0 # Outer iteration count.
    maxMVP = 2000
    notConvergeFlag = False

    
    if fl == 1: print 'it in. it res'
    while rout > rt: # Outer iteration
        print 'Outer: rout=%g, rt=%g' % (rout, rt);

        if MVP > maxMVP:
            print '#Matrix-vector products > %d. This process probably will not converge. Residual: %f' \
                % (maxMVP, rout)
            notConvergeFlag = True
            break
        
        i = i + 1
        k = 0
        y = e 
        innertol = max(eta ** 2 * rout, rt)
        
        while rho_km1 > innertol: #Inner iteration by CG
            #print '    Inner: rho_km1=%g, innertol=%g, rt=%g' % (rho_km1, innertol, rt)
            k = k + 1
            
            if k == 1:
                Z = rk / v 
                p = Z
                rho_km1 = rk.dot(Z)
            else:
                beta = rho_km1 / rho_km2
                p = Z + beta * p
            
            # Update search direction efficiently.
            w = x * A.dot(x * p) + v * p 
            alpha = rho_km1 / p.dot(w)
            ap = alpha * p

            # Test distance to boundary of cone.
            ynew = y + ap; 
            
            if min(ynew) <= delta:
                if delta == 0: break
                ind = np.where(ap < 0)[0]
                gamma = min((delta - y[ind]) / ap[ind]) # dense
                #gamma = min((sparse(delta)-y(ind))./ap(ind)); # sparse
                y = y + gamma * ap;
                break

            if max(ynew) >= Delta:
                ind =np.where(ynew > Delta)[0]
                gamma = min((Delta - y[ind]) / ap[ind]) # dense
                #gamma = min((sparse(Delta)-y(ind))./ap(ind)); # sparse
                y = y + gamma * ap;
                break

            y = ynew
            rk = rk - alpha * w
            rho_km2 = rho_km1
            Z = rk / v
            rho_km1 = rk.dot(Z)

            
        x = x * y
        v = x * A.dot(x)
        rk = 1.0 - v # dense
        #rk = sparse(1) - v; # sparse
        rho_km1 = rk.dot(rk)
        rout = rho_km1
        MVP = MVP + k + 1
        
        # Update inner iteration stopping criterion.
        rat = rout / rold
        rold = rout
        res_norm = np.sqrt(rout)
        eta_o = eta
        eta = g * rat

        if g * eta_o ** 2 > 0.1:
            eta = max(eta, g * eta_o ** 2)

        eta = max(min(eta, etamax), stop_tol / res_norm)

        if fl == 1:
            print '%3d %6d %.3e ' % (i, k, r_norm)
            res += [r_norm]

    print 'Matrix-vector products = %6d' % MVP
    
    return (x, res, notConvergeFlag)



def KRnormalize_sparse(in_file, n, outFile, biasFile):
# DepthCutOff must be > 0, and should be > 2.

    print 'Loading the sparse matrix'
    coo = pd.read_table(file_name, header=None, names=['row', 'col', 'val'],
                        dype={'row':int, 'col':int, 'val':float})
    # The output of juicebox pre would contains NaN
    coo = coo.dropna(subset=['val'])    
    csr_mat = sparse.coo_matrix((coo['val'], (coo['row'], coo['col'])),
                                shape=(n, n), dtype=float).tocsr()
    
    # Construct the lest elements form the lower/upper matrix
    csr_mat = csr_mat + csr_mat.transpose() - sparse.diags(csr_mat.diagonal())
    
    depth = np.sum(csr_mat, axis=1)
    valid = np.where(depth >= 2)[0]
    valid_csr_mat = csr_mat[valid, :].tocsc[:, valid].tocsr();

    print 'Normalizing'
    [x, res, isNotConverge] = bnewt_sparse(valid_csr_mat);

    if isNotConverge:
        print 'Warning: Not converged!!!'

    bias = np.empty(n) * np.nan
    bias[valid] = x    
    normed_mat = sparse.diags(bias, format="csr")\
                       .dot(csr_mat)\
                       .dot(sparse.diags(bias, format="csr")).tocoo()

    print 'Writing'
    fout = open(outFile, 'w')
    for (row, col, val) in zip(normed_mat.row, normed_mat.col, normed_mat.data):
        fout.write("{0:d} {1:d} {2:g}".format(row, col, val))

    np.savetxt(biasFile, bias, fmt='%g', delimiter=' ')
    


args = sys.argv
in_filename = args[1]
mat_size = args[2]
out_filename = args[3]
bias_filename = args[4]
KRnormalize_sparse(in_filename, mat_size, out_filename, bias_filename)
