/*
 * The collision of two LEF can makes a insulation boundary anywhere.
 * This is why it is very important to avoid the collisions.
 * On A. Sanborn, et al. (2015), one of two LEF disasociates when collision.
 * On M. Imakaev, et al. (2016), tuning death rate.

 *
 * A = Rad21 ChIP-seq profile / Input profile
 * Birth probability ~ A
 * Death probability is constant
 * Pause probability ~ (1 - the next value of A)
 * (Move probability ~ adjascent ChIP-seq signal)
 */

#include <iostream>
#include <fstream>
#include <map>
#include <memory>
#include <vector>
#include <random>
#include <algorithm>
#include <array>
#include <queue>
#include <limits>
#include "H5Cpp.h"




namespace lpext // LOop-EXTrusion
{
  double rand_prob() {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_real_distribution<> dist(0, 1);
    return dist(mt);
  }


  
  int rand_int(int start, int end) {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_int_distribution<> dist(start, end);
    return dist(mt);
  }


  
  std::vector<double> accumulate(std::vector<double> arr)
  {
    std::vector<double> cumulative_arr(arr.size(), 0);
    
    if (arr.size() == 0) return cumulative_arr;
    
    cumulative_arr[0] = arr[0];
    for (size_t i=1; i<arr.size(); i++)
      cumulative_arr[i] = cumulative_arr[i - 1] + arr[i];
    
    return cumulative_arr;
  }


  
  int sample_from_cumulative_prob(const std::vector<double>& cumulative_prob)
  {
    auto it = std::upper_bound(cumulative_prob.begin(), cumulative_prob.end(), rand_prob());
    auto pos = it - cumulative_prob.begin();
    return pos;
  }
  
  /***
   * !!!! ALL values have some units. !!!!
   * The default units are OpenMM are nanometers, picoseconds, amu, mol, kiloJoule and Kelvin.
   * Friction coefficient is in inverse picoseconds.
   * The potential in the reference paper is in [kB*T], but for OpenMM, in [kJ/mol].
   * The conversion is [kB*T] = kB * T * 1e-3 * avogadro_number [kJ/mol].
   * The unit of length is actually the diameter of a monomer (~10 nm),
   * so the following all [nm] units must be replased [10 nm].
   * I do not know why ~3 nucleosomes are ~10 nm....
   ***/  
  class Parameters
  {
  public:
    const std::string out_file;
    
    const std::string cohesin_exclusion_file = "/work2/yuichi/medaka/MZT/ChIP-seq/Rad21/exclusion/data/cohesin_exclusion_score/Rad21_FEnegLog_st12_step1000bp_window2kb_chr";
    //const std::string chip_seq_rad21_file = ;
    //const std::string chip_seq_input_file = ;
    
    const int bin_size = 1000; // [bp]
    const std::string chrom;// = "chr11";
    const int start;// = 10286215 - 500000; // [bp]
    const int end;// = 10793881 + 500000; // [bp]
    const int start_bin;// = start / bin_size;
    const int end_bin;// = (end - 1) / bin_size + 1;
    std::vector<double> cohesin_exclusion_scores;
    const int bin_size_exclusion_score = 1000;
    const double death_upper_bound;
    const double pause_upper_bound;
    
    
    const int non_saved_blocks = 200;//100;                            // Sup info
    const int saved_blocks = 1000;//500;                               // Sup info
    const int num_save_cycles = 100;//4; //10 //save 100 * 1000               // Sup info
    const int steps_per_block_1d = 4;                                  // Sup info
    const int relax_steps_1d = 40000;//00;                                // Sup info
    const int total_saved_blocks = saved_blocks * num_save_cycles;    
    const int total_steps_1d = steps_per_block_1d * (saved_blocks + non_saved_blocks)
                               * num_save_cycles + relax_steps_1d; 


    const double monomer_length = 1;                          // [kb]
    const int num_monomers;// = end_bin - start_bin;
    
    //const double processivity;// = 400; //400; //120                   // Data S1 Rnak1
    const double separation;// = 60; //120                       // Data S1 Rnak1
    const int num_lefs;// = num_monomers / separation; // Sup info
    //const double birth_prob = 1.0 / num_monomers;        // Sup information, uniform
    //const double death_prob;// = 2.0 / processivity; // Sup info, constant
    //const double stall_prob = 1;                         // Sup info, imparmeable
    //const double stalled_death_prob;// = death_prob;        // Sup info, constant
    //const double pause_prob = 0;                         // Sup info, not pause

    
    Parameters(const std::string& c, const int s, const int e,
               const std::string& of,
               const double given_separation,
               const double given_death_upper_bound,
               const double given_pause_upper_bound)
      : out_file(of), chrom(c), start(s), end(e),
        start_bin(start / bin_size), end_bin((end - 1) / bin_size + 1),
        death_upper_bound(given_death_upper_bound),
        pause_upper_bound(given_pause_upper_bound),
        num_monomers(end_bin - start_bin),
        separation(given_separation),
        num_lefs(num_monomers / separation)
    {
      // Read cohesin exclusion score
      cohesin_exclusion_scores = std::vector<double>(num_monomers,
                                                     std::numeric_limits<double>::quiet_NaN());
      std::ifstream ifs(cohesin_exclusion_file);
      while (!ifs.eof()) {
        std::string chr;
        int s, e;
        double score;
        ifs >> chr >> s >> e >> score;

        if (chrom == chr && start_bin * bin_size <= s && e <= end_bin * bin_size) {
          cohesin_exclusion_scores[s / bin_size - start_bin] = score;
        }
      }

      // Linear interpolate NaN
      for (size_t i=0; i < cohesin_exclusion_scores.size(); i++) {
        if (std::isnan(cohesin_exclusion_scores[i])) {
          int left_idx = -1;
          int right_idx = -1;
          int left = 0;
          int right = 0;
          
          for (int j=i-1; j >= 0; j--) {
            if (!std::isnan(cohesin_exclusion_scores[j])) {
              left_idx = j;
              left = cohesin_exclusion_scores[j];
              break;
            }
          }
          for (size_t j=i+1; j < cohesin_exclusion_scores.size(); j++) {
            if (!std::isnan(cohesin_exclusion_scores[j])) {
              right_idx = j;
              right = cohesin_exclusion_scores[j];
              break;
            }
          }
          
          if (left_idx < 0 && right_idx < 0) {
            std::cerr << "Error: the cohesin exclusion profile are all NaN." << std::endl;
            std::exit(1);
          }
          else if (left_idx < 0) left = right;
          else if (right_idx < 0) right = left;

          // Interpolate
          for (int j=left_idx+1; j < right_idx; j++) {
            cohesin_exclusion_scores[j]
              = left + (j - left_idx) * (right - left) / (right_idx - left_idx); 
          }
        }
      }

      // shift to positive
      auto min = *std::min_element(cohesin_exclusion_scores.begin(), cohesin_exclusion_scores.end());
      for (size_t i=0; i < cohesin_exclusion_scores.size(); i++) {
        cohesin_exclusion_scores[i] =  cohesin_exclusion_scores[i] - min + 1;
      }
    }
  };
  

  
  class LEF // Loop-Extrusing Factor
  {
  public:
    int pos[2];
    bool stalled_left=false, stalled_right=false; 
    
    LEF() {}
    LEF(int l, int r) : pos{l, r} {}
  };
  

  
  class Simulator_1D
  {
    const std::shared_ptr<const lpext::Parameters> params;
    std::vector<LEF> lefs;
    std::vector<bool> occupied;
    std::vector<double> birth_prob, death_prob, stalled_death_prob,
      pause_prob_from_left, pause_prob_from_right,
      stall_prob_from_left, stall_prob_from_right, cumulative_birth_prob;

 
    void birth(std::vector<lpext::LEF>::iterator& lef_itr)
    {
      if (std::find(occupied.begin(), occupied.end(), false) == occupied.end()) {
        std::cerr << "Error: Fail to LEF birth because all position occupied by LEFs." << std::endl;
        std::exit(1);
      }

      std::random_device rnd;
      std::mt19937 mt(rnd());
      std::uniform_real_distribution<> dist(0, 1);

      // [0, 0.5) => same pos. [0.5, 1) => addjascent pos.
      while (true) {
        // Inverse transform sampling from birth probability
        auto pos = sample_from_cumulative_prob(cumulative_birth_prob);
        if (dist(mt) < 0.5) { // Birth at same position
          if (occupied[pos]) continue;
          *lef_itr = lpext::LEF(pos, pos);
          break;
        }
        else { // Birth at addjascent positions.
          if (pos >= params->num_monomers - 1 || occupied[pos] || occupied[pos + 1]) {
            // The last condition means that BE between pos and pos + 1
            continue;
          }
          
          *lef_itr = lpext::LEF(pos, pos + 1);
          break;
        }
      }
      
      occupied[lef_itr->pos[0]] = true;
      occupied[lef_itr->pos[1]] = true;
      return;
    }


    void death(std::vector<lpext::LEF>::iterator& lef_itr)
    {
      occupied[lef_itr->pos[0]] = false;
      occupied[lef_itr->pos[1]] = false;
      birth(lef_itr); // Assuming #LEF is constant.
    }

    
  public:
    Simulator_1D(const std::shared_ptr<const lpext::Parameters>& p)
      : params(p),
        lefs(p->num_lefs),
        occupied(p->num_monomers, false),
        birth_prob(p->num_monomers, 0),
        death_prob(p->num_monomers, 0),
        stalled_death_prob(p->num_monomers, 0),
        pause_prob_from_left(p->num_monomers, 0),
        pause_prob_from_right(p->num_monomers, 0),
        stall_prob_from_left(p->num_monomers, 0),
        stall_prob_from_right(p->num_monomers, 0)
    {
            std::cout << "Init 1D" << std::endl;
      // Init birth, death, stall, and pause probability
      if (p->bin_size_exclusion_score != p->monomer_length * 1000) {
        std::cerr << "Error: Bin size of exclusion score and monomer length are different. "
                  << p->bin_size_exclusion_score
                  << " "
                  << p->monomer_length * 1000
                  << std::endl;
        std::exit(1);
      }

      double total = 0;
      for (size_t i=0; i < birth_prob.size(); i++) {
        birth_prob[i] = -p->cohesin_exclusion_scores[i];
        //total += birth_prob[i];
      }

      auto min = *std::min_element(birth_prob.begin(), birth_prob.end());
      for (size_t i=0; i < birth_prob.size(); i++) {
        birth_prob[i] = birth_prob[i] - min + 1; // to positive values
        total += birth_prob[i];
      }

      for (size_t i=0; i < birth_prob.size(); i++) {
        birth_prob[i] /= total;
      }
      
      //std::fill(birth_prob.begin(), birth_prob.end(), p->birth_prob);         // constant
      //std::fill(birth_prob.begin(), birth_prob.end(), 1.0 / p->num_monomers);

      //std::fill(death_prob.begin(), death_prob.end(), p->death_prob);         // constant
      for (size_t i=0; i < birth_prob.size(); i++) {
        //death_prob[i] = std::min(1.0,
        //                         std::pow(2, p->cohesin_exclusion_scores[i]) / p->upper_bound_score);
        death_prob[i] = std::min(1.0,
                                 p->cohesin_exclusion_scores[i] / p->death_upper_bound);
      }

      //std::fill(stalled_death_prob.begin(), stalled_death_prob.end(),
      //          p->stalled_death_prob); // constact

      
      //std::fill(pause_prob_from_left.begin(), pause_prob_from_left.end(), p->pause_prob);
      //std::fill(pause_prob_from_right.begin(), pause_prob_from_right.end(), p->pause_prob);
      /*
      for (size_t i=0; i < pause_prob_from_left.size() - 1; i++)
        if (p->cohesin_exclusion_scores[i + 1] > p->upper_bound_score)
          pause_prob_from_left[i] = 1;

      for (size_t i=1; i < pause_prob_from_right.size(); i++)
        if (p->cohesin_exclusion_scores[i - 1] > p->upper_bound_score)
          pause_prob_from_right[i] = 1;
      */

      pause_prob_from_left[pause_prob_from_left.size() - 1] = 0;
      for (size_t i=1; i < pause_prob_from_left.size(); i++) {
        //pause_prob_from_left[i]
        //  = std::min(1.0,
        //             std::pow(2, p->cohesin_exclusion_scores[i + 1]) / p->upper_bound_score);
        pause_prob_from_left[i]
          = std::min(1.0,
                     p->cohesin_exclusion_scores[i + 1] / p->pause_upper_bound);
      }
      pause_prob_from_right[0] = 0;
      for (size_t i=1; i < pause_prob_from_right.size(); i++) {
        //pause_prob_from_right[i]
        //  = std::min(1.0,
        //             std::pow(2, p->cohesin_exclusion_scores[i - 1]) / p->upper_bound_score);
        pause_prob_from_right[i]
          = std::min(1.0,
                     p->cohesin_exclusion_scores[i - 1] / p->pause_upper_bound);
      }
      //std::fill(pause_prob_from_left.begin(), pause_prob_from_left.end(), 0);
      //std::fill(pause_prob_from_right.begin(), pause_prob_from_right.end(), 0);

      cumulative_birth_prob = accumulate(birth_prob);

      // Init LEFs. Assuming the constant number of LEFs
      for (auto itr=lefs.begin(); itr!=lefs.end(); ++itr) birth(itr);
    }

    void step()
    {
      std::random_device rnd;
      std::mt19937 mt(rnd());
      std::uniform_real_distribution<> dist(0, 1);

      
      // Directional motion
      for (auto itr = lefs.begin(); itr != lefs.end(); ++itr) {
        auto death_prob_left = itr->stalled_left ? stalled_death_prob[itr->pos[0]]
                                                : death_prob[itr->pos[0]];
        auto death_prob_right = itr->stalled_right ? stalled_death_prob[itr->pos[1]]
                                                  : death_prob[itr->pos[1]];
        
        if (std::max(death_prob_left, death_prob_right) == 1 ||
            rand_prob() < std::max(death_prob_left, death_prob_right)) {
          death(itr);
        }
        else { 
          // Does the left arm move?
          if (stall_prob_from_right[itr->pos[0]] > 0 &&
              dist(mt) <= stall_prob_from_right[itr->pos[0]]) {
            itr->stalled_left = true;
          }
          else if (itr->pos[0] > 0 &&
                   !occupied[itr->pos[0] - 1] &&
                   (pause_prob_from_right[itr->pos[0]] == 0 ||
                    dist(mt) > pause_prob_from_right[itr->pos[0]])) {
            // Not stalled, paused, and occupied. Progressive move.
            if (itr->pos[0] != itr->pos[1])
              occupied[itr->pos[0]] = false;
            occupied[itr->pos[0] - 1] = true;
            itr->pos[0] -= 1;
            itr->stalled_left = false;
          }
          
          // Does the right arm move?
          if ((stall_prob_from_left[itr->pos[1]] > 0 &&
               dist(mt) <= stall_prob_from_left[itr->pos[1]])) {
            itr->stalled_right = true;
          }
          else if (itr->pos[1] < params->num_monomers - 1 &&
                   !occupied[itr->pos[1] + 1] &&
                   (pause_prob_from_left[itr->pos[1]] == 0 ||
                    dist(mt) > pause_prob_from_left[itr->pos[1]])) {
            // Not stalled, paused, and occupied. Progressive move.
            if (itr->pos[0] != itr->pos[1])
              occupied[itr->pos[1]] = false;
            occupied[itr->pos[1] + 1] = true;
            itr->pos[1] += 1;
            itr->stalled_right = false;          
          }
        }
      }

      /*
      // Random motion
      for (auto itr = lefs.begin(); itr != lefs.end(); ++itr) {
        auto death_prob_left = itr->stalled_left ? stalled_death_prob[itr->pos[0]]
                                                 : death_prob[itr->pos[0]];
        auto death_prob_right = itr->stalled_right ? stalled_death_prob[itr->pos[1]]
                                                  : death_prob[itr->pos[1]];
        
        if (std::max(death_prob_left, death_prob_right) == 1 ||
            rand_prob() < std::max(death_prob_left, death_prob_right)) {
          death(itr);
        }
        else { 
          // Does the left arm move?
          if (stall_prob_from_right[itr->pos[0]] > 0 &&
              dist(mt) <= stall_prob_from_right[itr->pos[0]]) {
            itr->stalled_left = true;
          }
          else if (dist(mt) < 0.5) { // move to Left
            if (itr->pos[0] > 0 &&
                !occupied[itr->pos[0] - 1] &&
                (pause_prob_from_right[itr->pos[0]] == 0 ||
                 dist(mt) > pause_prob_from_right[itr->pos[0]])) {
              if (itr->pos[0] != itr->pos[1])
                occupied[itr->pos[0]] = false;
              occupied[itr->pos[0] - 1] = true;
              itr->pos[0] -= 1;
              itr->stalled_left = false;
            }
          }
          else { // move to right
            if (itr->pos[0] < params->num_monomers - 1 &&
                !occupied[itr->pos[0] + 1] &&
                (pause_prob_from_left[itr->pos[0]] == 0 ||
                 dist(mt) > pause_prob_from_left[itr->pos[0]])) {
              // Not stalled, paused, and occupied. Progressive move.
              if (itr->pos[0] != itr->pos[1])
              occupied[itr->pos[0]] = false;
              occupied[itr->pos[0] + 1] = true;
              itr->pos[0] += 1;
              itr->stalled_right = false;          
            }
          }

          // Does the right arm move?
          if ((stall_prob_from_left[itr->pos[1]] > 0 &&
               dist(mt) <= stall_prob_from_left[itr->pos[1]])) {
            itr->stalled_right = true;
          }
          else if (dist(mt) < 0.5) { // move to Left
            if (itr->pos[1] > 0 &&
                !occupied[itr->pos[1] - 1] &&
                (pause_prob_from_right[itr->pos[1]] == 0 ||
                 dist(mt) > pause_prob_from_right[itr->pos[1]])) {
              if (itr->pos[0] != itr->pos[1])
                occupied[itr->pos[1]] = false;
              occupied[itr->pos[1] - 1] = true;
              itr->pos[1] -= 1;
              itr->stalled_left = false;
            }
          }
          else { // move to right
            if (itr->pos[1] < params->num_monomers - 1 &&
                !occupied[itr->pos[1] + 1] &&
                (pause_prob_from_left[itr->pos[1]] == 0 ||
                 dist(mt) > pause_prob_from_left[itr->pos[1]])) {
              // Not stalled, paused, and occupied. Progressive move.
              if (itr->pos[0] != itr->pos[1])
              occupied[itr->pos[1]] = false;
              occupied[itr->pos[1] + 1] = true;
              itr->pos[1] += 1;
              itr->stalled_right = false;          
            }
          }

        }
      }
      */
    }

    
    void steps(int n)
    {
      for (int i=0; i<n; i++) step();
    }

    
    std::vector<std::array<int, 2>> get_lef_positions() const
    {
      std::vector<std::array<int, 2>> poses;
      for (const auto& lef : lefs) {
        poses.push_back({{lef.pos[0], lef.pos[1]}});
      }
      return poses;
    }

 

    void save_lef_positions(H5::H5File file, std::string data_name) const
    {
      auto positions = get_lef_positions();
      
      // Create data set
      auto nrow = params->num_lefs;
      auto ncol = 2;
      hsize_t data_dim[2]{hsize_t(nrow), hsize_t(ncol)};
      H5::DataSpace data_space(2, data_dim);
      auto data_set = file.createDataSet(data_name.c_str(), H5::PredType::NATIVE_INT, data_space);
      
      // For HDF5, the array must have a contiguous memory.
      // So, it cannot recognize a pointer array, e.g. std::vector and double**.
      int *data = new int[nrow * ncol];
      
      for (int i = 0; i < nrow; i++) {
        for (int j = 0; j < ncol; j++) {
          data[i * ncol + j] = positions[i][j];
        }
      }

      data_set.write(data, H5::PredType::NATIVE_INT);
      delete data;
    }
  };

    
  
  
  class Loop_extrusion_simulator
  {
    const std::shared_ptr<const lpext::Parameters> params;
    lpext::Simulator_1D sim_1d;
    std::vector<std::vector<std::array<int, 2>>> lef_positions_time_course;
    std::vector<int> current_active_lef_bond_idxs;
    std::map<std::array<int, 2>, int> bond_to_idx;
        
  public:
    Loop_extrusion_simulator(const std::shared_ptr<const lpext::Parameters> p)
      : params(p), sim_1d(p) {}

    
    void run()
    {
      H5::H5File h5_file(params->out_file.c_str(), H5F_ACC_TRUNC);
      int num_saved_blocks_1d = 0;
      bool first_1d = true;
      
      std::cout << "Run LEF dynamics" << std::endl;
      
      while (num_saved_blocks_1d < params->total_saved_blocks) {
        for (int i=0; i < params->non_saved_blocks; i++) {
          sim_1d.steps(params->steps_per_block_1d);
          lef_positions_time_course.push_back(sim_1d.get_lef_positions());
        }
        
        for (int i=0; i < params->saved_blocks; i++) {
          if (num_saved_blocks_1d >= params->total_saved_blocks) break;

          sim_1d.steps(params->steps_per_block_1d);
          lef_positions_time_course.push_back(sim_1d.get_lef_positions());

          sim_1d.save_lef_positions(h5_file, std::to_string(num_saved_blocks_1d) + "_LEF");
          num_saved_blocks_1d += 1;
        }

        if (first_1d) {
          // Relax and equilibrate
          // No store and save because of much many steps.
          sim_1d.steps(params->relax_steps_1d);
          first_1d = false;
        }
      }      
    }
  };
}




int main(int argc, char *argv[])
{
  if (argc != 8) {
    std::cerr << "#Parameters must be 7" << std::endl;
    return -1;
  }

  const std::string chrom = argv[1];
  const int start = std::atoi(argv[2]); // [bp]
  const int end = std::atoi(argv[3]); // [bp]
  const std::string out_file = argv[4];
  // Parameters
  const double separation = std::atof(argv[5]);
  const double death_upper_bound = std::atof(argv[6]);
  const double pause_upper_bound = std::atof(argv[7]);
  

  std::cout << "Start simulation" << std::endl;
    const std::shared_ptr<const lpext::Parameters>
    params(new lpext::Parameters(chrom, start, end, out_file,
                                 separation, death_upper_bound, pause_upper_bound));
  std::cout << "Init" << std::endl;
  const std::unique_ptr<lpext::Loop_extrusion_simulator>
    sim(new lpext::Loop_extrusion_simulator(params));

  sim->run();

  std::cout << "End simulation" << std::endl;

  return 0;
}


