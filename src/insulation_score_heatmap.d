/* iS: insulation score
 * This program uses a full matrix.
 * For medaka chromosome, the maximum memory requirement is <512 MB in 5kb res.
 */

import std.stdio, std.conv, std.string, std.range, std.algorithm, std.math;


void main(string[] args)
{
  auto hicFile = args[1];
  auto squareSize = args[2].to!uint; // [bins]
  auto unobsThresholdRate = args[3].to!double; // Remove iS with > unobservable lines at square
  auto sparsity = args[4].to!double; // [bins] High sparses row of a contact map are suspected.
  auto smoothingWindow = args[5].to!uint; // [bins]
  auto outFile = args[6];
  
  int unobservable = (unobsThresholdRate * squareSize).to!int;
  
  auto hicMatrix = readHicMatrix(hicFile);
  auto iSProfile = computeISProfile(hicMatrix,
                                    squareSize, unobservable,
                                    sparsity, smoothingWindow);
  writeISProfile(outFile, iSProfile);
}


void writeISProfile(string outFile, ISProfile iSProfile)
{
  File fout;
  if (outFile == "/dev/stdout") fout = stdout;
  else fout = File(outFile, "w");
  
  fout.writeln(iSProfile.toString);
}


double sparsity(double[] row)
{
  return row.filter!(x => x == 0).array.length.to!double / row.length;
}


ISProfile computeISProfile(double[][] hicMatrix,
                           uint squareSize, uint unobservable,
                           double sparsity, uint smoothingWindow)
{
  auto iSProfile = new ISProfile(squareSize);

  // The rows with all zero have sparsity 1.
  auto observableFlags = hicMatrix.map!(row => row.sparsity <= sparsity).array;  
  
  foreach (i; 0..hicMatrix.length) {
    auto iS = computeIS(hicMatrix, i, squareSize,
                        observableFlags, unobservable);//, assembledFlags);
    iSProfile.addIS(iS);
  }

  // Convert iS == 0 to NaN
  iSProfile.zeroToNan;
  iSProfile.smoothing(smoothingWindow);
  iSProfile.normalize;
  
  return iSProfile;
}



double computeIS(double[][] hicMatrix, size_t i,
                 uint squareSize, bool[] observableFlags,
                 uint unobservable)//, bool[] assembledFlags)
{
  // case: The insulation square comes out of the matrix.
  if (i < squareSize || hicMatrix.length <= i + squareSize)
    return double.nan; // Not defined

  // This case splits a single peak to two peaks at a single gap.
  // case: This bin is not observable.
  // if (!observableFlags[i]) return double.nan;

  // case: The insulation square contains #bins >= unobservable.
  auto u = 0;
  foreach (k; 1..(squareSize + 1)) {
    if (!observableFlags[i - k]) u++;
    if (!observableFlags[i + k]) u++;
  }
  if (u > unobservable)
    return double.nan;

  // The following case is included in the above unobservable case.
  // case: The insulation square is across a misassembled-ish junction.
  //       Detect > unobservable continuous misassembled-ish region.
  /*
  foreach (k; 1..(squareSize + 1)) {
    if (assembledFlags[i] != assembledFlags[i - k]) return double.nan;
    if (assembledFlags[i] != assembledFlags[i + k]) return double.nan;
  }
  */

  double iS = 0;
  auto n = 0;

  foreach (p; 1..(squareSize + 1)) {
    foreach (q; 1..(squareSize + 1)) {
      if (observableFlags[i - p] && observableFlags[i + q]) {
        iS += hicMatrix[i - p][i + q];
        n++;
      }
    }
  }

  if (n == 0) return double.nan;
  else return iS / n.to!double; // Mean of values in square
}



class ISProfile
{
  private uint squareSize;
  private double[] profile;

  this (uint ss)
  {
    squareSize = ss;
  }

  void addIS(double iS)
  {
    profile ~= iS;
  }

  void setIS(double iS, size_t i)
  {
    if (i >= profile.length)
      throw new Exception("The index is out of range.");
    profile[i] = iS;
  }

  void zeroToNan()
  {
    profile = profile.map!(iS => iS == 0 ? double.nan : iS).array;
  }

  void smoothing(uint smoothingWindow)
  {
    if (smoothingWindow == 0) return;
    if ((smoothingWindow - 1) % 2 != 0)
      throw new Exception("Smoothing window must be 2 * n + 1");

    auto extend = (smoothingWindow - 1) / 2;
    auto centerStart = extend; // center of window
    auto centerEnd = profile.length - extend; // half-open [)

    auto smoothedProfile = new double[](profile.length);
    smoothedProfile.fill(double.nan);
    
    foreach (i; centerStart..centerEnd) {
      auto smoothedIS = double.nan;

      if (profile[i] !is double.nan) {
        auto windowStart = i - extend;
        auto windowEnd = i + extend + 1;
        auto windowProfile = profile[windowStart..windowEnd].filter!(iS => iS !is double.nan).array;

        // If the number of double.nan > (window size - 1) / 2, ignore this IS.
        // It means #double.nan must be <= extend.
        // smoothingWindow - #double.nan >= smoothingWindow - extend = extend + 1
        if (windowProfile.length > extend) {
          smoothedIS = windowProfile.sum / windowProfile.length;
        }
      }

      smoothedProfile[i] = smoothedIS;
    }

    profile = smoothedProfile;
  }
  
  void normalize()
  {
    auto noNan = profile.filter!(iS => iS !is double.nan).array;
    auto logMean = log2(noNan.sum / noNan.length);
    
    foreach (ref iS; profile) iS = log2(iS) - logMean;
  }

  override string toString() {
    return profile.map!(x => x.to!string).array.join(" ");
  }
}



double[][] readHicMatrix(string hicFile)
{
  double[][] hicMatrix;

  foreach (line; File(hicFile).byLineCopy) {
    auto fields = line.strip.split;
    hicMatrix ~= fields.map!(x => x.to!double).array;
  }

  return hicMatrix;
}
