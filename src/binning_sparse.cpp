// Binning sparse matrix
#include <fstream>
#include <vector>
#include <map>
#include <array>


std::map<std::array<int, 2>, double> read_sparse_matrix(std::string filename)
{
  auto ifs = std::ifstream(filename);
  std::map<std::array<int, 2>, double> sp_mat;
  int i = -1;
  int j = -1;
  double val=-1;
  
  while (ifs) {
    ifs >> j >> i >> val;
    sp_mat[std::array<int, 2>{{i, j}}] = val;
  }

  return sp_mat;
}


std::map<std::array<int, 2>, double>
binning_sparse(std::map<std::array<int, 2>, double>& sp_mat, int num_binning)
{
  std::map<std::array<int, 2>, double> binned_sp_mat;
  int binned_i = -1;
  int binned_j = -1;

  for (const auto& entry : sp_mat) {
    binned_i = entry.first[0] / num_binning;
    binned_j = entry.first[1] / num_binning;
    binned_sp_mat[std::array<int, 2>{{binned_i, binned_j}}] = entry.second;
  }
  
  return binned_sp_mat;
}



void write_sparse_matrix(std::string filename, std::map<std::array<int, 2>, double>& sp_mat,
                         std::string delim=" ")
{
  std::ofstream ofs(filename);
  for (const auto& entry : sp_mat)
    ofs << entry.first[0] << delim
        << entry.first[1] << delim
        << entry.second << "\n";
  ofs.close();
}



int main(int argc, char* argv[])
{
  const std::string in_filename = argv[1];
  const int num_binning = std::atoi(argv[2]);
  const std::string out_filename = argv[3];

  auto contact_freq_map = read_sparse_matrix(in_filename);
  auto binned_contact_freq_map = binning_sparse(contact_freq_map, num_binning);
  write_sparse_matrix(out_filename, binned_contact_freq_map);
  
  return 0;
}

