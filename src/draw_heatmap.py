import numpy as np
import pandas as pd
import sys
from scipy import sparse
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# I have not controled the position of the yaxis ticks
# at the right side of an axis in matplotlib 2.0.2.
# I cannot set the direction of yticks at the right "in".
# Then I set these global parameter as matplotlib 1.5.1.
plt.rcParams['ytick.right'] = True
plt.rcParams['xtick.direction'] = 'out'
plt.rcParams['ytick.direction'] = 'out'

'''
def read_triplet(file_name, size):
    # Loading KR normalized contact map
    coo = pd.read_table(file_name, header=None, sep=" ",
                        names=['row', 'col', 'val'],
                        dtype={'row':int, 'col':int, 'val':float})
    # The output of juicebox pre contains NaN
    coo = coo.dropna(subset=['val'])
    
    mat = sparse.coo_matrix((coo['val'], (coo['row'], coo['col'])), shape=(size, size))
    mat = np.array(mat.todense())
    mat = mat + mat.T - np.diag(np.diag(mat))
    
    unobserved = np.where(mat.sum(axis=1) == 0)
    if len(unobserved) != 0:
        mat[unobserved, :] = np.NaN
        mat[:, unobserved] = np.NaN
        
    return mat
'''

def read_dense_matrix(file_name):
    mat = np.loadtxt(file_name, delimiter=" ")
    unobserved = np.where(mat.sum(axis=1) == 0)
    if len(unobserved) != 0:
        mat[unobserved, :] = np.NaN
        mat[:, unobserved] = np.NaN
        
    return mat

    

args = sys.argv
in_filename = args[1]
#num_monomers = int(args[2])
#out_filename = args[3]
out_filename = args[2]

print 'Loading'
#mat = read_triplet(in_filename, num_monomers)
mat = read_dense_matrix(in_filename)

triu_arr = mat[np.triu_indices_from(mat, 1)]
#vmin = np.nanpercentile(triu_arr, 0)
vmin = 0
vmax = np.nanpercentile(triu_arr, 95)
print vmin
print vmax

from matplotlib.colors import LinearSegmentedColormap
kr_cmap = LinearSegmentedColormap.from_list('krcmap', ['white', 'red'])
kr_cmap.set_bad('white', 1.)
font_size = 10

print 'Drawing'
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
heatmap = ax.matshow(mat, cmap=kr_cmap, vmin=vmin, vmax=vmax)
#heatmap = ax.matshow(mat, cmap=kr_cmap)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = plt.colorbar(mappable=heatmap, cax=cax)
#ax.set(xticks=[], yticks=[])
#ax.set_title(stage, size=font_size)
#ax.set_ylabel('Contact map', size=font_size)
plt.savefig(out_filename, format = 'png', dpi=1200, bbox_inches="tight", pad_inches=0.2)
plt.close()
