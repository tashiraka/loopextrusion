/*
 * 1. Read a contact map.
 * 2. Compute a contact probability
 * 3. Write the summed contact map to a file as triplet format.
 *
 * These are not implemented.
 * - Normalization
 * - Contact probability P(s) calculation
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include "H5Cpp.h"


// Assume the format used in juicebox
std::vector<std::vector<double>> read_cmap_triplet(const std::string& filename, const int bin_size,
                                                    const int start_bin, const int end_bin)
{
  int mat_size = end_bin - start_bin;
  std::vector<std::vector<double>> cmap(mat_size, std::vector<double>(mat_size, 0));
  
  std::ifstream ifs(filename);
  int start1, start2;
  double val;
  
  while (!ifs.eof()) {
    ifs >> start1 >> start2 >> val;
    start1 = start1 / bin_size;
    start2 = start2 / bin_size;

    if (start_bin <= start1 && start1 <= end_bin &&
        start_bin <= start2 && start2 <= end_bin) {
      cmap[start1][start2] = val;
    }
  }

  return cmap;
}


std::vector<std::vector<double>> read_cmap_full(const std::string& filename,
                                                 const int start_bin, const int end_bin)
{
  int mat_size = end_bin - start_bin;
  std::vector<std::vector<double>> cmap(mat_size, std::vector<double>(mat_size, 0));
  
  std::ifstream ifs(filename);
  std::string line;
  int row_idx = 0;
  
  while (std::getline(ifs, line)) {
    std::istringstream iss(line);
    std::vector<std::string> fields;
    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              std::back_inserter(fields));
    
    if (start_bin <= row_idx && row_idx < end_bin) {
      for (int col_idx=0; col_idx < fields.size(); col_idx++) {
        if (start_bin <= col_idx && col_idx < end_bin) {
          cmap[row_idx][col_idx] = std::stod(fields[col_idx]);
        }
      }
    }
    row_idx++;
  }

  return cmap;
}


std::vector<double> compute_contact_probability(const std::vector<std::vector<double>>& cmap)
{
  std::vector<double> cprob(cmap.size(), 0);

  for (size_t i=0; i < cmap.size(); i++) {
    for (size_t j=0; j <= i; j++) {
      cprob[i - j] += cmap[i][j];
    }
  }
  
  return cprob;
}


void write_vec(std::string filename, std::vector<double>& vec)
{
  std::ofstream ofs(filename);
  for (const auto& elem : vec) {
    ofs << elem << "\n";
  }
  ofs.close();
}


int main(int argc, char* argv[])
{

  
  // cmap is contact frequency matrix
  const std::string cmap_file = argv[1];
  const std::string cmap_format = argv[2]; // "full" or "triplet"
  const int start = std::atoi(argv[3]);
  const int end = std::atoi(argv[4]);
  const int bin_size = std::atoi(argv[5]);
  const std::string out_filename = argv[6];

  const int start_bin = start / bin_size;
  const int end_bin = (end - 1) / bin_size + 1;

  std::vector<std::vector<double>> cmap;
  
  if (cmap_format == "full") {
    cmap = read_cmap_full(cmap_file, start_bin, end_bin);
  }
  else if (cmap_format == "triplet") {
    cmap = read_cmap_triplet(cmap_file, bin_size, start_bin, end_bin);
  }
  else {
    std::cerr << "Error: The format of contact map is \"full\" or \"triplet\"" << std::endl;
    exit(1);
  }

  auto cprob = compute_contact_probability(cmap);

  write_vec(out_filename, cprob);
  return 0;
}
