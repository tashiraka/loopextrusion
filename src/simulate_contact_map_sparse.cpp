/*
 * 1. Read the single conformation from HDF5 file.
 * 2. Compute a contact map from the conformation 
 *    and sum it into a population contac map by using sparse matrix.
 * 3. Write the summed contact map to a file as triplet format.
 *
 * These are not implemented.
 * - Normalization
 * - Contact probability P(s) calculation
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <map>
#include <utility>
#include "H5Cpp.h"


std::vector<std::vector<double>>
read_matrix_from_hdf5(const H5::H5File& file, const std::string& dataset_name)
{
  H5::DataSet dataset = file.openDataSet(dataset_name);
  H5::DataSpace dataspace = dataset.getSpace();
  hsize_t dims_out[2];
  dataspace.getSimpleExtentDims(dims_out, NULL);
  size_t nrow = dims_out[0];
  size_t ncol = dims_out[1];
  double* buf = new double[nrow * ncol];
  dataset.read(buf, H5::PredType::NATIVE_DOUBLE);
  std::vector<std::vector<double>> mat(nrow, std::vector<double>(ncol));
  for (size_t row_idx=0; row_idx < nrow; row_idx++)
    for (size_t col_idx=0; col_idx < ncol; col_idx++)
      mat[row_idx][col_idx] = buf[row_idx * ncol + col_idx];
  return mat;
}


double euclidean_distance_3d(const std::vector<double>& u, const std::vector<double>& v)
{
  double d = 0;
  for (int i=0; i<3; i++) {
    int dif = u[i] - v[i];
    d += dif * dif;
  }
  return d;
}


void sum_contact(std::map<std::pair<int, int>, double>& lower_contact_freq_map,
                 const std::vector<std::vector<double>>& xyz,
                 const double capture_radius)
{
  for (size_t i=0; i < xyz.size(); i++){  
    lower_contact_freq_map[std::make_pair(i, i)] += 1;
  }
  
  for (size_t i=0; i < xyz.size(); i++){
    for (size_t j=0; j < i; j++){
      if (euclidean_distance_3d(xyz[i], xyz[j]) <= capture_radius) {
        lower_contact_freq_map[std::make_pair(i, j)] += 1;
      }
    }
}
}


int main(int argc, char* argv[])
{
  const std::string in_filename = argv[1];
  const int total_saved_blocks = std::atoi(argv[2]);
  const std::string out_filename = argv[3];
  const double capture_radius = 10; // [nm]
  std::map<std::pair<int, int>, double> lower_contact_freq_map;

  H5::H5File file(in_filename, H5F_ACC_RDONLY);
    
  for (int block_num=0; block_num < total_saved_blocks; block_num++) {
    std::cout << block_num << std::endl;
    std::string dataset_name = std::to_string(block_num);
    auto xyz = read_matrix_from_hdf5(file, dataset_name);
    sum_contact(lower_contact_freq_map, xyz, capture_radius);
  }

  std::ofstream ofs(out_filename);
  for (const auto& entry : lower_contact_freq_map) {
    auto row_idx = entry.first.first;
    auto col_idx = entry.first.second;
    auto freq = entry.second;
    ofs << row_idx << " " << col_idx << " " << freq << "\n";
  }
  ofs.close();
  
  return 0;
}
