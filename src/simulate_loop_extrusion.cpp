/*
 * The collision of two LEF can makes a insulation boundary anywhere.
 * This is why it is very important to avoid the collisions.
 * On A. Sanborn, et al. (2015), one of two LEF disasociates when collision.
 * On M. Imakaev, et al. (2016), tuning death rate.
 */

#include <iostream>
#include <fstream>
#include <map>
#include <memory>
#include <vector>
#include <random>
#include <algorithm>
#include <array>
#include <queue>
#include "H5Cpp.h"
#include "OpenMM.h"




namespace lpext // LOop-EXTrusion
{
  double rand_prob() {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_real_distribution<> dist(0, 1);
    return dist(mt);
  }


  
  int rand_int(int start, int end) {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_int_distribution<> dist(start, end);
    return dist(mt);
  }


  
  std::vector<double> accumulate(std::vector<double> arr)
  {
    std::vector<double> cumulative_arr(arr.size(), 0);
    
    if (arr.size() == 0) return cumulative_arr;
    
    cumulative_arr[0] = arr[0];
    for (size_t i=1; i<arr.size(); i++)
      cumulative_arr[i] = cumulative_arr[i - 1] + arr[i];
    
    return cumulative_arr;
  }


  
  int sample_from_cumulative_prob(const std::vector<double>& cumulative_prob)
  {
    auto it = std::upper_bound(cumulative_prob.begin(), cumulative_prob.end(), rand_prob());
    auto pos = it - cumulative_prob.begin();
    return pos;
  }
  
  /***
   * !!!! ALL values have some units. !!!!
   * The default units are OpenMM are nanometers, picoseconds, amu, mol, kiloJoule and Kelvin.
   * Friction coefficient is in inverse picoseconds.
   * The potential in the reference paper is in [kB*T], but for OpenMM, in [kJ/mol].
   * The conversion is [kB*T] = kB * T * 1e-3 * avogadro_number [kJ/mol].
   * The unit of length is actually the diameter of a monomer (~10 nm),
   * so the following all [nm] units must be replased [10 nm].
   * I do not know why ~3 nucleosomes are ~10 nm....
   ***/  
  class Parameters
  {
  public:
    const std::string out_file;
    const std::string plugin_dir; // path to OpenMM plugin

    const int non_saved_blocks = 200; //100                            // Sup info
    const int saved_blocks = 1000; //500                               // Sup info
    const int num_save_cycles = 10; //4                                // Sup info
    const int steps_per_block_1d = 4;                                  // Sup info
    const double step_ratio_3d_by_1d = 5000.0 / 4.0;                   // Data S1 Rnak1
    const int num_steps_3d = step_ratio_3d_by_1d * steps_per_block_1d; // Sup info
    const int relax_steps_1d = 4000000;                                // Sup info
    const int total_saved_blocks = saved_blocks * num_save_cycles;    
    const int total_steps_1d = steps_per_block_1d * (saved_blocks + non_saved_blocks)
                               * num_save_cycles + relax_steps_1d; 


    const double monomer_length = 0.6;                          // [kb] Sup info
    const int num_monomers = (300 + 600 + 1200) * 8;//2;        // Sup info
    /*
    const std::array<int, 5> int boundaries{{300,
                               300 + 600,
                               300 + 600 + 1200,
                               300 + (300 + 600 + 1200),
                               300 + 600 + (300 + 600 + 1200)}};
    */
    const std::array<int, 23> boundaries{
         {300,
          300 + 600,
          300 + 600 + 1200,
          300 + (300 + 600 + 1200),
          300 + 600 + (300 + 600 + 1200),
          2 * (300 + 600 + 1200),
          300 + 2 * (300 + 600 + 1200),
          300 + 600 + 2 * (300 + 600 + 1200),
          3 * (300 + 600 + 1200),
          300 + 3 * (300 + 600 + 1200),
          300 + 600 + 3 * (300 + 600 + 1200),
          4 * (300 + 600 + 1200),
          300 + 4 * (300 + 600 + 1200),
          300 + 600 + 4 * (300 + 600 + 1200),
          5 * (300 + 600 + 1200),
          300 + 5 * (300 + 600 + 1200),
          300 + 600 + 5 * (300 + 600 + 1200),
          6 * (300 + 600 + 1200),
          300 + 6 * (300 + 600 + 1200),
          300 + 600 + 6 * (300 + 600 + 1200),
          7 * (300 + 600 + 1200),
          300 + 7 * (300 + 600 + 1200),
          300 + 600 + 7 * (300 + 600 + 1200)}}; // Sup info
    
    const double separation = 120;                       // [kb] Data S1 Rnak1
    const double processibility = 120;                   // [kb] Data S1 Rnak1
    const int num_lefs = num_monomers / (separation / monomer_length); // Sup info
    const double birth_prob = 1.0 / num_monomers;        // Sup information, uniform
    const double death_prob = 2.0 / (processibility / monomer_length); // Sup info, constant
    const double stall_prob = 1;                         // Sup info, imparmeable
    const double stalled_death_prob = death_prob;        // Sup info, constant
    const double pause_prob = 0;                         // Sup info, not pause

    const std::string platform_name = "CUDA";
    const std::string precision = "mixed";
    const std::string integrator_name = "langevin";
    const double temperature = 300;            // [K] Bitbucket openmmlib
    const double friction_coeff = 0.01; //40;  // [ps^-1] Bitbucket openmmlib
    const double time_step = 0.08; //0.008;    // [ps] Bitbucket newSimulatorCode
    
    const double kB = 1.380658e-23;            // [J/K] openmm/reference/SimTKOpenMMRealType.h
    const double avogadro = 6.0221367e23;      // [mol^-1] openmm/reference/SimTKOpenMMRealType.h
    const double kBT_to_kJPerMol = kB * temperature * avogadro * 1e-3; // Bitbucket openmmlib,

    const double radius = 1.0;                 // [nm]
    const double mass = 100;                   // [amu] Bitbucket openmmlib
    const double density = 0.2;                // Data S1
    const double box_size = std::pow(num_monomers / density, 1.0 / 3.0); // [nm] Sup info

    const double initial_bond_length = radius;            // [nm] Sup info, 3D lattice
    const double bond_k = 100 * kBT_to_kJPerMol;          // [kJ/mol/nm^2] Sup info
    const double bond_natural_length = radius;            // [nm] Sup info
    const double lef_bond_k = 4 * kBT_to_kJPerMol; // [kJ/mol/nm^2] Sup info
    const double lef_bond_natural_length = radius;        // [nm] Sup info
    const double stiff_k = 2 * kBT_to_kJPerMol;           // [kJ/mol/nm^2] Data S1 Rank 1

    
    Parameters(const std::string& of, const std::string& p_path)
      : out_file(of), plugin_dir(p_path) {}
  };
  

  
  class LEF // Loop-Extrusing Factor
  {
  public:
    int pos[2];
    bool stalled_left=false, stalled_right=false; 
    
    LEF() {}
    LEF(int l, int r) : pos{l, r} {}
  };
  

  
  class Simulator_1D
  {
    const std::shared_ptr<const lpext::Parameters> params;
    std::vector<LEF> lefs;
    std::vector<bool> occupied;
    std::vector<double> birth_prob, death_prob, stalled_death_prob, pause_prob,
      stall_prob_from_left, stall_prob_from_right, cumulative_birth_prob;

 
    void birth(std::vector<lpext::LEF>::iterator& lef_itr)
    {
      if (std::find(occupied.begin(), occupied.end(), false) == occupied.end()) {
        std::cerr << "Error: Fail to LEF birth because all position occupied by LEFs." << std::endl;
        std::exit(1);
      }

      std::random_device rnd;
      std::mt19937 mt(rnd());
      std::uniform_real_distribution<> dist(0, 1);

      // [0, 0.5) => same pos. [0.5, 1) => addjascent pos.
      while (true) {
        // Inverse transform sampling from birth probability
        auto pos = sample_from_cumulative_prob(cumulative_birth_prob);

        if (dist(mt) < 0.5) { // Birth at same position
          if (occupied[pos]) continue;
          *lef_itr = lpext::LEF(pos, pos);
          break;
        }
        else { // Birth at addjascent positions.
          if (pos >= params->num_monomers - 1 || occupied[pos] || occupied[pos + 1] ||
              std::find(params->boundaries.begin(), params->boundaries.end(), pos + 1) !=
              params->boundaries.end()) // The last condition means that BE between pos and pos + 1
            continue;
          
          *lef_itr = lpext::LEF(pos, pos + 1);
          break;
        }
      }
      
      occupied[lef_itr->pos[0]] = true;
      occupied[lef_itr->pos[1]] = true;
      return;
    }


    void death(std::vector<lpext::LEF>::iterator& lef_itr)
    {
      occupied[lef_itr->pos[0]] = false;
      occupied[lef_itr->pos[1]] = false;
      birth(lef_itr); // Assuming #LEF is constant.
    }

    
  public:
    Simulator_1D(const std::shared_ptr<const lpext::Parameters>& p)
      : params(p),
        lefs(p->num_lefs),
        occupied(p->num_monomers, false),
        birth_prob(p->num_monomers, 0),
        death_prob(p->num_monomers, 0),
        stalled_death_prob(p->num_monomers, 0),
        pause_prob(p->num_monomers, 0),
        stall_prob_from_left(p->num_monomers, 0),
        stall_prob_from_right(p->num_monomers, 0)
    {
      // Init birth, death, stall, and pause probability
      std::fill(birth_prob.begin(), birth_prob.end(), p->birth_prob);         // uniform
      std::fill(death_prob.begin(), death_prob.end(), p->death_prob);         // constant
      std::fill(stalled_death_prob.begin(), stalled_death_prob.end(), p->stalled_death_prob); // constact
      std::fill(pause_prob.begin(), pause_prob.end(), p->pause_prob);         // constant
      for (const auto& pos: p->boundaries) {
        // A boundary position pos is between monomer positions pos-1 and pos 
        stall_prob_from_left[pos-1] = p->stall_prob; // stall when --> pos-1 | pos
        stall_prob_from_right[pos] = p->stall_prob; // stall when pos-1 | pos <--
      }
      cumulative_birth_prob = accumulate(birth_prob);

      // Init LEFs. Assuming the constant number of LEFs
      for (auto itr=lefs.begin(); itr!=lefs.end(); ++itr) birth(itr);
    }

    
    void step()
    {
      std::random_device rnd;
      std::mt19937 mt(rnd());
      std::uniform_real_distribution<> dist(0, 1);

      // Directional motion
      for (auto itr=lefs.begin(); itr != lefs.end(); ++itr) {
        auto death_prob_left = itr->stalled_left ? stalled_death_prob[itr->pos[0]]
                                                : death_prob[itr->pos[0]];
        auto death_prob_right = itr->stalled_right ? stalled_death_prob[itr->pos[1]]
                                                  : death_prob[itr->pos[1]];
        
        if (std::max(death_prob_left, death_prob_right) == 1 ||
            rand_prob() < std::max(death_prob_left, death_prob_right)) {
          death(itr);
        }
        else { 
          // Does the left arm move?
          if (stall_prob_from_right[itr->pos[0]] > 0 &&
              dist(mt) <= stall_prob_from_right[itr->pos[0]]) {
            itr->stalled_left = true;
          }
          else if (itr->pos[0] > 0 &&
                   !occupied[itr->pos[0] - 1] &&
                   (pause_prob[itr->pos[0]] == 0 ||
                    dist(mt) > pause_prob[itr->pos[0]])) {
            // Not stalled, paused, and occupied. Progressive move.
            if (itr->pos[0] != itr->pos[1])
              occupied[itr->pos[0]] = false;
            occupied[itr->pos[0] - 1] = true;
            itr->pos[0] -= 1;
            itr->stalled_left = false;
          }
          
          // Does the right arm move?
          if ((stall_prob_from_left[itr->pos[1]] > 0 &&
               dist(mt) <= stall_prob_from_left[itr->pos[1]])) {
            itr->stalled_right = true;
          }
          else if (itr->pos[1] < params->num_monomers - 1 &&
                   !occupied[itr->pos[1] + 1] &&
                   (pause_prob[itr->pos[1]] == 0 ||
                    dist(mt) > pause_prob[itr->pos[1]])) {
            // Not stalled, paused, and occupied. Progressive move.
            if (itr->pos[0] != itr->pos[1])
              occupied[itr->pos[1]] = false;
            occupied[itr->pos[1] + 1] = true;
            itr->pos[1] += 1;
            itr->stalled_right = false;          
          }
        }
      }
    }

    
    void steps(int n)
    {
      for (int i=0; i<n; i++) step();
    }

    
    std::vector<std::array<int, 2>> get_lef_positions() const
    {
      std::vector<std::array<int, 2>> poses;
      for (const auto& lef : lefs) {
        poses.push_back({{lef.pos[0], lef.pos[1]}});
      }
      return poses;
    }

 

    void save_lef_positions(H5::H5File file, std::string data_name) const
    {
      auto positions = get_lef_positions();
      
      // Create data set
      auto nrow = params->num_lefs;
      auto ncol = 2;
      hsize_t data_dim[2]{hsize_t(nrow), hsize_t(ncol)};
      H5::DataSpace data_space(2, data_dim);
      auto data_set = file.createDataSet(data_name.c_str(), H5::PredType::NATIVE_INT, data_space);
      
      // For HDF5, the array must have a contiguous memory.
      // So, it cannot recognize a pointer array, e.g. std::vector and double**.
      int *data = new int[nrow * ncol];
      
      for (int i = 0; i < nrow; i++) {
        for (int j = 0; j < ncol; j++) {
          data[i * ncol + j] = positions[i][j];
        }
      }

      data_set.write(data, H5::PredType::NATIVE_INT);
      delete data;
    }
  };

    
  
  class Simulator_3D
  {
    const std::shared_ptr<const lpext::Parameters> params;
    OpenMM::System* system = nullptr;
    OpenMM::Platform* platform = nullptr;
    OpenMM::LangevinIntegrator* integrator = nullptr;
    OpenMM::Context* context = nullptr;
    
    // The Forces must not be deleted by myself. System object delete them.
    OpenMM::HarmonicBondForce* bond_force = nullptr;
    OpenMM::HarmonicBondForce* lef_bond_force = nullptr;
    OpenMM::CustomAngleForce* stiff_force = nullptr;
    OpenMM::CustomNonbondedForce* repul_force = nullptr;

    
    std::vector<OpenMM::Vec3> random_compact_positions()
    {
      /***
       * Discritize box size for calculation.
       * Transform params->initial_bond_length to 1
       * For example, when box_size = 1 and initial_bonx_length = 0.3,
       * virtual_box_size = 3 and grids are 0, 1, 2 (= 0, 0,3, 0,6, 0.9).
       * Reverse=transfrom is just * 0.3.
       ***/
      int virtual_box_size = params->box_size / params->initial_bond_length;
      
      // Compact initial conformation in [1, virtual_box_size]^3
      std::vector<std::array<int, 3>> virtual_positions;
      std::vector<std::vector<std::vector<bool>>>
        occupied(virtual_box_size,
                 std::vector<std::vector<bool>>(virtual_box_size,
                                                std::vector<bool>(virtual_box_size, false)));
      
      auto mid = virtual_box_size / 2;

      // Seed polymer
      std::cout << "Seed polymer" << std::endl;
      for (int i=1; i<virtual_box_size-1; i++) {
        virtual_positions.push_back({{mid, mid, i}});
        occupied[mid][mid][i] = true;
      }

      for (int i=virtual_box_size-2; i>0; i--) {
        virtual_positions.push_back({{mid, mid + 1, i}});
        occupied[mid][mid + 1][i] = true;        
      }

      // Grow the seed
      std::cout << "Grow the seed" << std::endl;
      
      for (int i=virtual_positions.size()+1; i<=params->num_monomers; i++) {
        while (true) {
          // Select a growing position
          auto grow_pos = rand_int(0, virtual_positions.size() - 2);
          auto pos1 = virtual_positions[grow_pos];
          auto pos2 = virtual_positions[grow_pos + 1];

          int bond_direction = 0; // 0, 1, or 2 is x, y, or z
          for (int j=0; j<3; j++) {
            if (pos1[j] != pos2[j]) {
              bond_direction = j;
              break;
            }
          }
          
          // Select a growing direction
          int axis = bond_direction;
          while (axis == bond_direction) axis = rand_int(0, 2);
          int sign = rand_int(0, 1) == 0 ? 1 : -1;
          
          std::array<int, 3> new_pos1(pos1);
          std::array<int, 3> new_pos2(pos2);
          new_pos1[axis] = new_pos1[axis] + sign;
          new_pos2[axis] = new_pos2[axis] + sign;

          if (*std::min_element(new_pos1.begin(), new_pos1.end()) > 0 &&
              *std::min_element(new_pos2.begin(), new_pos2.end()) > 0 &&
              *std::max_element(new_pos1.begin(), new_pos1.end()) < virtual_box_size &&
              *std::max_element(new_pos2.begin(), new_pos2.end()) < virtual_box_size &&
              !occupied[new_pos1[0]][new_pos1[1]][new_pos1[2]] &&
              !occupied[new_pos2[0]][new_pos2[1]][new_pos2[2]]) {
            virtual_positions.insert(virtual_positions.begin() + grow_pos + 1, {new_pos1, new_pos2});
            occupied[new_pos1[0]][new_pos1[1]][new_pos1[2]] = true;
            occupied[new_pos2[0]][new_pos2[1]][new_pos2[2]] = true;
            break;
          }
        }
      }

      // Reverse-transform
      std::vector<OpenMM::Vec3> positions(params->num_monomers);
      for (int i=0; i<params->num_monomers; i++) {
        for (int j=0; j<3; j++) {
          positions[i][j] = virtual_positions[i][j] * params->initial_bond_length;
        }
      }

      return positions;
    }

    
    // Sampled from Maxwell distribution
    std::vector<OpenMM::Vec3> random_velocities()
    {
      auto std_dev = sqrt(params->kB * params->temperature / params->mass) * 1e-3; // [nm / ps]
      std::random_device rnd;
      std::mt19937 mt(rnd());
      std::normal_distribution<double> normal(0, std_dev);
      std::vector<OpenMM::Vec3> velocities(params->num_monomers);

      for (auto&& v : velocities) {
        for (int i=0; i<3; i++) {
          v[i] = normal(mt);
        }
      }
      
      return velocities;
    }

    
  public:
    ~Simulator_3D()
    {
      delete context;
      delete system;
      delete platform;
      delete integrator;
    }

    
    Simulator_3D(const std::shared_ptr<const lpext::Parameters>& p)
      : params(p)
    {
      // System
      system = new OpenMM::System();

      system->setDefaultPeriodicBoxVectors({p->box_size, 0, 0},
                                           {0, p->box_size, 0},
                                           {0, 0, p->box_size});

      for (int i=0; i<p->num_monomers; i++) system->addParticle(p->mass);

      // Forces
      // Bonds
      bond_force = new OpenMM::HarmonicBondForce();
      for (int i=1; i<p->num_monomers; i++)
        bond_force->addBond(i - 1, i, p->bond_natural_length, p->bond_k);
      system->addForce(bond_force);
            
      // Repulsion
      const std::string repul_force_eq =
        "rsc12 * (rsc2 - 1.0) * REPe / REPemin + REPe;"
        "rsc12 = rsc4 * rsc4 * rsc4;"
        "rsc4 = rsc2 * rsc2;"
        "rsc2 = rsc * rsc;"
        "rsc = r / REPsigma * REPrmin;";
      repul_force = new OpenMM::CustomNonbondedForce(repul_force_eq);
      repul_force->addGlobalParameter("REPe", 1.5 * p->kBT_to_kJPerMol);
      const double r_with_zero_potensial = 1.05;
      repul_force->addGlobalParameter("REPsigma", r_with_zero_potensial);
      repul_force->addGlobalParameter("REPemin", 46656.0 / 823543.0);
      repul_force->addGlobalParameter("REPrmin", sqrt(6.0 / 7.0));
      repul_force->setCutoffDistance(r_with_zero_potensial);
      repul_force->setNonbondedMethod(repul_force->CutoffPeriodic); // Periodic boundary condition
      for (int i=0; i<p->num_monomers; i++) repul_force->addParticle();
      for (int i=1; i<p->num_monomers; i++) repul_force->addExclusion(i - 1, i); 
      system->addForce(repul_force);

      // Stiffness
      const std::string angle_force_eq = "stiff_k * (1 - cos(theta))";
      stiff_force = new OpenMM::CustomAngleForce(angle_force_eq);
      stiff_force->addGlobalParameter("stiff_k", p->stiff_k);
      for (int i=2; i < p->num_monomers; i++)
        stiff_force->addAngle(i - 2, i - 1, i);
      system->addForce(stiff_force);

      // Platform
      OpenMM::Platform::loadPluginsFromDirectory(params->plugin_dir);

      // Accept CUDA and CPU
      auto platform_name_lower = p->platform_name;
      std::transform(platform_name_lower.begin(), platform_name_lower.end(),
                     platform_name_lower.begin(), ::tolower);
      
      if (platform_name_lower == "cuda") {
        platform = &OpenMM::Platform::getPlatformByName("CUDA");
        platform->setPropertyDefaultValue("CudaPrecision", p->precision);
      }
      else if (platform_name_lower == "cpu") {
        platform = &OpenMM::Platform::getPlatformByName("CPU");
      }
      else {
        std::cerr << "Error: Accepting only CUDA or CPU" << std::endl;
        std::exit(1);
      }

      
      // Integrator
      auto integrator_name_lower = p->integrator_name;
      std::transform(integrator_name_lower.begin(), integrator_name_lower.end(),
                     integrator_name_lower.begin(), ::tolower);

      // Accept only langevin
      if (integrator_name_lower == "langevin") {
        integrator = new OpenMM::LangevinIntegrator(p->temperature, p->friction_coeff,
                                                    p->time_step);
      }
      else {
        std::cerr << "Error: Accepting only langeivn" << std::endl;
        std::exit(1);
      }
    }


    void init(const std::vector<std::vector<std::array<int, 2>>>& lef_positions_time_course,
              std::map<std::array<int, 2>, int>& bond_to_idx)
    {
      init_LEF_bonds(lef_positions_time_course, bond_to_idx);
      init_context();
    }

    
    // All LEF bonds set inactive
    void init_LEF_bonds(const std::vector<std::vector<std::array<int, 2>>>& lef_positions_time_course,
                        std::map<std::array<int, 2>, int>& bond_to_idx)
    {
      // Remove duplicates
      std::vector<std::array<int, 2>> unique_pos;
      for(const auto& lef_positions : lef_positions_time_course)
        unique_pos.insert(end(unique_pos), begin(lef_positions), end(lef_positions));
      std::sort(unique_pos.begin(), unique_pos.end());
      unique_pos.erase(std::unique(unique_pos.begin(), unique_pos.end()), unique_pos.end());
      
      // Add all LEF bonds in whole LEF dynamics
      if (lef_bond_force == nullptr) {
        lef_bond_force = new OpenMM::HarmonicBondForce();

        int bond_idx = 0;
        for (const auto& pos : unique_pos) {
          lef_bond_force->addBond(pos[0], pos[1], params->lef_bond_natural_length, 0);
          bond_to_idx[pos] = bond_idx;
          bond_idx++;
        }
          
        system->addForce(lef_bond_force);
      }
      else {
        std::cerr << "Error: Harmonic bonds by LEFs are already set." << std::endl;
        std::exit(1);
      }
    }


    void init_context()
    {
      context = new OpenMM::Context(*system, *integrator, *platform);
      context->setPositions(random_compact_positions());
      context->setVelocities(random_velocities());

      auto state = get_state();
      std::cout << "Initial kinetics: " <<
        state.getKineticEnergy() / params->num_monomers / params->kBT_to_kJPerMol << std::endl;
      std::cout << "Initail potential: " <<
        state.getPotentialEnergy() / params->num_monomers / params->kBT_to_kJPerMol << std::endl;
    }


    void update_LEF_bonds(const std::vector<std::array<int, 2>>& lef_positions,
                          const std::map<std::array<int, 2>, int>& bond_to_idx,
                          std::vector<int>& current_active_lef_bond_idxs)
    {
      // The bonded particles cannot be changed in OpenMM.
      // Inactivate the current LEF bonds
      if (current_active_lef_bond_idxs.empty()) {
          current_active_lef_bond_idxs.resize(lef_positions.size());
          std::fill(current_active_lef_bond_idxs.begin(), current_active_lef_bond_idxs.end(), -1);
      }
      else {
        for (size_t i=0; i < lef_positions.size(); i++) {
          int p1, p2;
          double l, k;
          
          auto cur_idx = current_active_lef_bond_idxs[i];
          lef_bond_force->getBondParameters(cur_idx, p1, p2, l, k);
          lef_bond_force->setBondParameters(cur_idx, p1, p2, l, 0);
        }
      }

      // Activate the new LEF bonds
      for (size_t i=0; i < lef_positions.size(); i++) {
        auto new_idx = bond_to_idx.at(lef_positions[i]);
        lef_bond_force->setBondParameters(new_idx, lef_positions[i][0], lef_positions[i][1],
                                          params->lef_bond_natural_length, params->lef_bond_k);
        current_active_lef_bond_idxs[i] = new_idx;
      }

      lef_bond_force->updateParametersInContext(*context);
    }

    
    void steps(int num_steps)
    {
      // No local energy minimization
      integrator->step(num_steps);

      auto state = get_state();
      std::cout <<
        "Kinetics: " <<
        state.getKineticEnergy()  / params->num_monomers / params->kBT_to_kJPerMol <<
        " Potential: " <<
        state.getPotentialEnergy() / params->num_monomers / params->kBT_to_kJPerMol <<
        std::endl;
    }

    
    OpenMM::State get_state() const
    {
      return context->getState(OpenMM::State::Positions |
                               OpenMM::State::Velocities |
                               OpenMM::State::Forces |
                               OpenMM::State::Energy);
    }

    
    std::vector<OpenMM::Vec3> get_positions() const
    {
      return context->getState(OpenMM::State::Positions).getPositions();
    }

    
    void save_conformation(H5::H5File file, std::string data_name) const
    {
      auto positions = get_positions();
      
      // Create data set
      auto nrow = params->num_monomers;
      auto ncol = 3;
      hsize_t data_dim[2]{hsize_t(nrow), hsize_t(ncol)};
      H5::DataSpace data_space(2, data_dim);
      auto data_set = file.createDataSet(data_name.c_str(), H5::PredType::NATIVE_DOUBLE, data_space);
      
      // For HDF5, the array must have a contiguous memory.
      // So, it cannot recognize a pointer array, e.g. std::vector and double**.
      double *data = new double[nrow * ncol];
      
      for (int i = 0; i < params->num_monomers; i++) {
        for (int j = 0; j < 3; j++) {
          data[i * ncol + j] = positions[i][j];
        }
      }

      data_set.write(data, H5::PredType::NATIVE_DOUBLE);
      delete data;
    }
  };


  
  class Loop_extrusion_simulator
  {
    const std::shared_ptr<const lpext::Parameters> params;
    lpext::Simulator_1D sim_1d;
    lpext::Simulator_3D sim_3d;
    std::vector<std::vector<std::array<int, 2>>> lef_positions_time_course;
    std::vector<int> current_active_lef_bond_idxs;
    std::map<std::array<int, 2>, int> bond_to_idx;
    
    void do_block(int num_blocks)
    {
      sim_3d.update_LEF_bonds(lef_positions_time_course[num_blocks], bond_to_idx,
                              current_active_lef_bond_idxs);
      sim_3d.steps(params->num_steps_3d);
    }

    
  public:
    Loop_extrusion_simulator(const std::shared_ptr<const lpext::Parameters> p)
      : params(p), sim_1d(p), sim_3d(p) {}

    
    void run()
    {
      H5::H5File h5_file(params->out_file.c_str(), H5F_ACC_TRUNC);
      int num_saved_blocks_1d = 0;
      bool first_1d = true;
      
      std::cout << "Run LEF dynamics" << std::endl;
      
      while (num_saved_blocks_1d < params->total_saved_blocks) {
        for (int i=0; i < params->non_saved_blocks; i++) {
          sim_1d.steps(params->steps_per_block_1d);
          lef_positions_time_course.push_back(sim_1d.get_lef_positions());
        }
        
        for (int i=0; i < params->saved_blocks; i++) {
          if (num_saved_blocks_1d >= params->total_saved_blocks) break;

          sim_1d.steps(params->steps_per_block_1d);
          lef_positions_time_course.push_back(sim_1d.get_lef_positions());

          sim_1d.save_lef_positions(h5_file, std::to_string(num_saved_blocks_1d) + "_LEF");
          num_saved_blocks_1d += 1;
        }

        if (first_1d) {
          // Relax and equilibrate
          // No store and save because of much many steps.
          sim_1d.steps(params->relax_steps_1d);
          first_1d = false;
        }
      }
      
      std::cout << "Init 3D simulator" << std::endl;
      sim_3d.init(lef_positions_time_course, bond_to_idx);
        
      std::cout << "Run loop-extrusion model simulation" << std::endl;
      int num_saved_blocks_3d = 0;
      int num_blocks = 0;

      while (num_saved_blocks_3d < params->total_saved_blocks) {
        for (int i=0; i < params->non_saved_blocks; i++) {
          do_block(num_blocks++);
        }

        for (int i=0; i < params->saved_blocks; i++) {
          if (num_saved_blocks_3d >= params->total_saved_blocks) break;
          do_block(num_blocks++);
          sim_3d.save_conformation(h5_file, std::to_string(num_saved_blocks_3d));
          num_saved_blocks_3d++;
          std::cout << "Saved: " << num_saved_blocks_3d << std::endl;
        }
      }      
    }
  };
}



void simulate(const std::string& out_file,  const std::string& plugin_dir)
{  
  const std::shared_ptr<const lpext::Parameters>
    params(new lpext::Parameters(out_file, plugin_dir));
  const std::unique_ptr<lpext::Loop_extrusion_simulator>
    sim(new lpext::Loop_extrusion_simulator(params));

  sim->run();
}



int main(int argc, char *argv[])
{
  if (argc != 3) {
    std::cerr << "#Parameters must be 2" << std::endl;
    return -1;
  }
  
  const std::string out_file = argv[1];
  const std::string plugin_dir = argv[2];

  std::cout << "Start simulation" << std::endl;
  simulate(out_file, plugin_dir);
  std::cout << "End simulation" << std::endl;

  return 0;
}


