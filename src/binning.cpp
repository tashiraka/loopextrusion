// Binning full matrix

#include <fstream>
#include <vector>


std::vector<std::vector<double>> read_dense_matrix(std::string filename)
{
  auto ifs = std::ifstream(filename);
  std::string line;
  int n = 0;
  while(!std::getline(ifs, line, '\n').eof()) n++;

  ifs.close();
  ifs.open(filename);

  std::vector<std::vector<double>> mat(n, std::vector<double>(n));

  for (int i=0; i < n; i++)
    for (int j=0; j < n; j++)
      ifs >> mat[i][j];

  return mat;
}


std::vector<std::vector<double>> binning(std::vector<std::vector<double>>& mat, int num_binning)
{
  size_t n = mat.size();
  size_t binned_n = n / num_binning;
  if (n % num_binning != 0) binned_n += 1;
  std::vector<std::vector<double>> binned_mat(binned_n, std::vector<double>(binned_n, 0));
  
  for (size_t i=0; i < mat.size(); i++) {
    for (size_t j=0; j < i+1; j++) {
      size_t binned_i = i / num_binning;
      size_t binned_j = j / num_binning;
      binned_mat[binned_i][binned_j] += mat[i][j];
      binned_mat[binned_j][binned_i] += mat[j][i];
    }
  }

  return binned_mat;
}


void write_matrix(std::string filename, std::vector<std::vector<double>>& mat, std::string delim=" ")
{
  std::ofstream ofs(filename);
  for (const auto& row : mat) {
    if (!row.empty()) ofs << row[0];
    for (size_t i=1; i < row.size(); i++) ofs << delim << row[i];
    ofs << "\n";
  }
  ofs.close();
}


int main(int argc, char* argv[])
{
  const std::string in_filename = argv[1];
  const int num_binning = std::atoi(argv[2]);
  const std::string out_filename = argv[3];

  auto contact_freq_map = read_dense_matrix(in_filename);
  auto binned_contact_freq_map = binning(contact_freq_map, num_binning);
  write_matrix(out_filename, binned_contact_freq_map);
  
  return 0;
}

