import sys, tempfile, pymol, h5py
import numpy as np

args = sys.argv
input_hdf5, out_file = args[1:3]
num_saved = 500

h5file = h5py.File(input_hdf5, "r")

# Starting PyMOL
pymol.pymol_argv = ["pymol", "-qc"]
pymol.finish_launching()
cmd = pymol.cmd
pymol_obj_name = "mov"

# Create a temporary xyz file from each hdf5 datasets.
# Then load xyz file to an object.
"""
tmp_file = NamedTemporaryFile()

for saved_idx in range(0, num_saved):
    coords = h5file[str(saved_idx)].value

    for coord in coords:
        xyz_fmt_line = " ".join(["UNK"] + [str(x) for x in coord]) + "\n"
        tmp_file.write(xyz_fmt_line)

    cmd.load(tmp_file, pymol_obj_name, format="xyz")

tmp_file.close()


# Create movie
mpng(pymol_obj_name)
load small_test.xyz
for x in range(0, 600): cmd.bond('index ' + str(x), 'index ' + str(x+1))
spectrum
"""
    
h5file = h5py.File("init.hdf5", "r")
test_file = open("init.xyz", "w")
coords = h5file["0"].value
for coord in coords:
    xyz_fmt_line = " ".join(["UNK"] + [str(x) for x in coord]) + "\n"
    test_file.write(xyz_fmt_line)
