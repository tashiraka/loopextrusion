import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import sparse
import math

args = sys.argv

exp_cmap_file = args[1]
out_file = args[2]
chrom_size_file = args[3];
chrom = args[4] #1
start = args[5]
end = args[6]
bin_size = args[7]


############################
# Conatact probability
############################
def read_kr(file_name, chrom_size):
    # Loading KR normalized contact map
    coo = pd.read_table(file_name, header=None, names=['row', 'col', 'val'])
    # The output of juicebox pre contains NaN
    coo = coo.dropna(subset=['val'])

    # binning
    size = int((chrom_size - 1) / bin_size) + 1
    coo['row'] = coo['row'] / bin_size
    coo['col'] = coo['col'] / bin_size

    kr_mat = sparse.coo_matrix((coo['val'], (coo['row'], coo['col'])), shape=(size, size))
    kr_mat = np.array(kr_mat.todense())
    kr_mat = kr_mat + kr_mat.T - np.diag(np.diag(kr_mat))
    
    unobserved = np.where(kr_mat.sum(axis=1) == 0)
    if len(unobserved) != 0:
        kr_mat[unobserved, :] = np.NaN
        kr_mat[:, unobserved] = np.NaN

    return kr_mat
chrom_sizes = pd.read_table(chrom_size_file,
                            header=None, names=['chrom', 'size'])
chrom_size = chrom_sizes[chrom_sizes['chrom'] == str(chrom)].iloc[0,1]
start_bin = int(start / bin_size)
end_bin = int((end - 1) / bin_size) + 1
exp_cmap = read_kr(exp_cmap_file, chrom_size)[start_bin:end_bin, start_bin:end_bin]

def calc_mean_cprob(cmap):
    cprob = np.zeros(len(cmap))
    for s in range(len(cmap)):
        for i in range(len(cmap) - s):
            if (~np.isnan(cmap[i, i + s])):
                cprob[s] += cmap[i, i + s]
    cprob = [p / (s + 1) for s, p in enumerate(cprob)]
    return cprob

def calc_median_cprob(cmap):
    cprob = [[] for i in range(len(cmap))]
    for s in range(len(cmap)):
        for i in range(len(cmap) - s):
            if (~np.isnan(cmap[i, i + s])):
                cprob[s] += [cmap[i, i + s]]
    cprob = [np.median(p) for p in cprob]
    return cprob

def calc_mean_cprob_log_bin(cmap, log_base=1.2):
    max_exp = int(math.log(len(cmap), log_base))
    # Remove the diagonal bins because they usually depends on the preprocess.
    log_bins = [int(1 * log_base ** e) for e in range(0, max_exp + 1)] + [len(cmap)]
    log_bins = np.unique(log_bins)

    cprob = [[] for i in range(len(cmap))]
    for s in range(len(cmap)):
        for i in range(len(cmap) - s):
            if (~np.isnan(cmap[i, i + s])):
                cprob[s] += [cmap[i, i + s]]

    log_cprob = [[] for i in range(len(log_bins) - 1)]
    s = log_bins[0]

    for i in range(len(log_bins) - 1):
        while (log_bins[i] <= s and s < log_bins[i + 1]):
            log_cprob[i] += cprob[s]
            s += 1
            
    log_cprob = [np.mean(p) for p in log_cprob]
    return log_cprob

# Remove the diagonal bins because they usually depends on the preprocess.
mean_cprob = calc_mean_cprob(exp_cmap)[1:]
median_cprob = calc_median_cprob(exp_cmap)[1:]
mean_log_cprob = calc_mean_cprob_log_bin(exp_cmap)


fig = plt.figure()
ax = fig.add_subplot(2,1,1)
ax.loglog(range(1, len(mean_cprob) + 1), mean_cprob, basex=1.2, basey=np.e, label='mean')
ax.loglog(range(1, len(median_cprob) + 1), median_cprob, basex=1.2, basey=np.e, label='median')
ax.set_xlabel('Distance along genome')
ax.set_ylabel('Contact probability')
ax.legend()
ax.grid()
ax = fig.add_subplot(2,1,2)
ax.semilogy(range(1, len(mean_log_cprob) + 1), mean_log_cprob, basey=np.e, label='mean log bin')
ax.set_xlabel('Distance along genome')
ax.set_ylabel('Contact probability')
ax.legend()
ax.grid()
plt.savefig(out_file, format='png', bbox_inches="tight", pad_inches=0.2)
plt.close()
