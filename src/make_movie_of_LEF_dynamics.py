import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import sys, h5py
import numpy as np
import matplotlib.animation as animation


args = sys.argv
input_hdf5 = args[1]
start_step = int(args[2])
end_step = int(args[3])
start_pos = int(args[4])
end_pos = int(args[5])
boundaries = args[6].strip(',')

if boundaries == "":
    boundaries = []
else:
    boundaries = [int(x) for x in args[6].split(',')]
out_file = args[7]

alpha = 0.6
height_scale = 1.0
max_tad_size = 1200
xmin = start_pos
xmax = end_pos
ymin = 0
ymax = height_scale * (end_pos - start_pos) / 10.0 #400 #height_scale * max_tad_size
linewidth = 0.5
interval = 50
aspect = 2
dpi = 250
font_size = 6

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

# Load HDF5 file and plot a arc
h5file = h5py.File(input_hdf5, "r")

def func(i):
    if i != 0:
        plt.cla()
    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])
    ax.set_aspect(aspect)
    ax.set_yticks([])
    ax.set_xticks(boundaries)
    ax.tick_params(axis='x', labelsize=font_size)
    #ax.axhline(y=0, xmin=xmin, xmax=xmax, color='black', linewidth=linewidth)
    for b in boundaries:
        ax.axvline(x=b, ymin=0, ymax=ymax, color='black', linewidth=linewidth)

    data_num = i + start_step
    positions = h5file[str(data_num) + "_LEF"].value
    positions = [pos for pos in positions \
                 if start_pos<=pos[0] and pos[0]<end_pos and \
                    start_pos<=pos[1] and pos[1]<end_pos]

    for pos in positions:
        center = (pos[0] + pos[1]) / 2.0
        width = np.abs(pos[0] - pos[1])
        height = height_scale * width
        a = mpatches.Arc(xy=(center, 0),
                         width=width, height=height,
                         linewidth=linewidth,
                         theta1=0, theta2=180,
                         ec='orange', alpha=alpha)
        ax.add_patch(a)


ani = animation.FuncAnimation(fig, func, interval=interval, frames=end_step - start_step)
ani.save(out_file, writer='imagemagick', dpi=dpi)
