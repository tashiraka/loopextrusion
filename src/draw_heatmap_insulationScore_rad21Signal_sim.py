import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as grd
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.patches import Rectangle
import numpy as np
from scipy import ndimage
from scipy import sparse
import pandas as pd
import sys
import math


# Parameters
args = sys.argv
# Assuming all matrices saved by dense matrix format
contact_map_file = args[1]
chrom = args[2]
bin_size = int(args[3])
start_sim = int(args[4])
end_sim = int(args[5])
start = int(args[6])  # [bp]
end = int(args[7]) # [bp]
left_margin = int(args[8]) # [bp]
right_margin = int(args[9])
insulation_score_file = args[10]
rad21_signal_file = args[11]
gene_file = args[12]
out_file = args[13]
num_binning = int(args[14])

# Binning the forcused region
start_margin = int(start / bin_size)
#end_margin = int((end - 1) / bin_size) + 1
start_target = int((start + left_margin) / bin_size)
end_target = int((end - right_margin - 1) / bin_size) + 1

start_c = start_sim / num_binning
end_c = end_sim / num_binning
#start_target - start_margin
#end_target - start_margin
start_i = start_sim / num_binning
end_i = end_sim / num_binning
start_g = start_target * bin_size
end_g = end_target * bin_size
start_e = start_sim
end_e = end_sim


start_p = start_target * bin_size + 1e4
end_p = end_target * bin_size + 1e4
step_p = 1e5
p_func = lambda x, pos: '%.2f Mb' % (x * 1e-7)


print "Read genes"
class Gene :
    def __init__(self, txStart, txEnd, cdsStart, cdsEnd, strand,
                 exonStarts, exonEnds, name, exonFrames):
        self.txStart = txStart
        self.txEnd = txEnd
        self.cdsStart = cdsStart
        self.cdsEnd = cdsEnd
        self.strand = strand
        self.exonStarts = exonStarts
        self.exonEnds = exonEnds
        self.name = name
        self.exonFrames = exonFrames
        
fin = open(gene_file, "r")
genes = []

for line in fin.xreadlines():
    fields = line.strip().split("\t")
    # GenePred is TAB separated
    # bin name chrom strand txStart txEnd cdsStart cdsEnd exsonCount exonStarts exonEnds score name2 cdsStartStat cdsEndStat exonFrame
    chrom_g = fields[2]
    strand_g = fields[3]
    txStart_g = int(fields[4])
    txEnd_g = int(fields[5])
    cdsStart_g = int(fields[6])
    cdsEnd_g = int(fields[7])
    exonStarts_g = [int(x) for x in fields[9].strip(",").split(",")]
    exonEnds_g = [int(x) for x in fields[10].strip(",").split(",")]
    name2_g = fields[12]
    exonFrames_g = [int(x) for x in fields[15].strip(",").split(",")]
    
    if chrom_g == chrom and (not txEnd_g <= start_g) and (not end_g <= txStart_g):
        genes += [Gene(txStart_g, txEnd_g, cdsStart_g, cdsEnd_g, strand_g,
                       exonStarts_g, exonEnds_g, name2_g, exonFrames_g)]

genes.sort(key=lambda x: x.txStart)


print "Read contact map"
# Load full contact map
contact = np.loadtxt(contact_map_file)[start_c:end_c, start_c:end_c]


print "Read rad21 signal"
rad21 = np.loadtxt(rad21_signal_file)


print "Read insulation"
insulation = np.loadtxt(insulation_score_file)


print "Drawing"
# Pre-calclate twidth
label_fontsize = 7
matplotlib.rcParams.update({'font.size': 6})
fig = plt.figure()
gs = grd.GridSpec(5, 2, height_ratios=[0.1, 2, 0.5, 1.2, 1], width_ratios=[50, 1],
                  hspace=0.15, wspace=0.05)
ax_p = fig.add_subplot(gs[0])
ax_c = fig.add_subplot(gs[2])
ax_c_cbar = fig.add_subplot(gs[3])
ax_g = fig.add_subplot(gs[4])
ax_i = fig.add_subplot(gs[6])
ax_i_cbar = fig.add_subplot(gs[7])
ax_e = fig.add_subplot(gs[8])                       

# Genes
txt_spacing_g = (end_g - start_g) / 1000
font_size_g = 3
renderer_g = fig.canvas.get_renderer()
rendered_axis_width_g = ax_g.get_window_extent().width
axis_width_g = end_g - start_g
rendered_to_coord_width = axis_width_g / rendered_axis_width_g
twidth_list = []
for gene in genes:
    tbox = ax_g.text(gene.txStart - txt_spacing_g, 0, gene.name,
                     ha="right", va="center",
                     zorder=10, fontsize=font_size_g, clip_on=True)
    twidth = tbox.get_window_extent(renderer_g).width * rendered_to_coord_width
    twidth_list += [twidth]
    
plt.close()


# Main Drawing
label_fontsize = 7
matplotlib.rcParams.update({'font.size': 6})
fig = plt.figure()
gs = grd.GridSpec(5, 2, height_ratios=[0.1, 1.8, 0.8, 1.2, 1], width_ratios=[50, 1],
                  hspace=0.15, wspace=0.05)
ax_p = fig.add_subplot(gs[0])
ax_c = fig.add_subplot(gs[2])
ax_c_cbar = fig.add_subplot(gs[3])
ax_g = fig.add_subplot(gs[4])
ax_i = fig.add_subplot(gs[6])
ax_i_cbar = fig.add_subplot(gs[7])
ax_e = fig.add_subplot(gs[8])                       


# Draw genomic postions
ax_p.spines['bottom'].set_visible(False)
ax_p.spines['right'].set_visible(False)
ax_p.spines['left'].set_visible(False)
ax_p.set_yticks([])
ax_p.set_xlim([start_g, end_g])
ax_p.xaxis.set_ticks_position('top')
ax_p.xaxis.set_ticks(np.arange(start_p, end_p, step_p))
ax_p.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(p_func))

# Draw the contact map
# Color map
kr_cmap = LinearSegmentedColormap.from_list('krcmap', ['white', 'red'])
kr_cmap.set_bad('white', 1.)

# Compute vmin and vmax by percentile
triu_arr_c = contact[np.triu_indices_from(contact, 1)]
vmin_c = np.nanpercentile(triu_arr_c, 0)
vmax_c = np.nanpercentile(triu_arr_c, 95)

# Rotate the coodinates 45 degree
sqrt2 = math.sqrt(2)
bin_coords = range(start_c, end_c)
X, Y = np.meshgrid(bin_coords, bin_coords)
sin45 = math.sin(math.radians(45))
X_, Y_ = X*sin45 + Y*sin45, X*sin45 - Y*sin45

# shift x coords to correct start coordinate and center
# the diagonal directly on the
X_ -= X_[1, 0] - (start_c - 1)
Y_ -= .5*np.min(Y_) + .5*np.max(Y_)

# Plot the sub-matrix
im_c = ax_c.pcolormesh(X_, Y_, contact, cmap=kr_cmap, vmin=vmin_c, vmax=vmax_c)

# Control axis
ylim_max = 0.2 * (end_c - start_c)
ax_c.set_ylim(-ylim_max, ylim_max)
ax_c.patch.set_visible(False)
ax_c.spines['top'].set_visible(False)
ax_c.spines['right'].set_visible(False)
ax_c.spines['left'].set_visible(False)
ax_c.spines['bottom'].set_visible(False)
ax_c.set_xticks([])
ax_c.set_yticks([])


# Draw color bar of contact map
cbar_c = plt.colorbar(mappable=im_c, cax=ax_c_cbar)


# Draw the insulation scores
plt.text(0.08, 1.1, 'Insulation score', ha='center', va='center',
         transform=ax_i.transAxes, fontsize=label_fontsize)
ax_i.set_ylabel("Window size [kb]", fontsize=label_fontsize)
im_i = ax_i.imshow(insulation[:25], cmap=plt.cm.RdBu_r, vmin=-0.5, vmax=0.5,
                   interpolation='nearest', aspect='auto', origin='lower')
ax_i.set_xticks([])
ax_i.set_xlim([start_i, end_i])
ax_i.set_yticks([0, 25])
ax_i.set_yticklabels(["30", "150"])


# Draw the color bar of insulation scores
cbar_i = plt.colorbar(mappable=im_i, cax=ax_i_cbar)


# Draw the rad21 signals
ax_e.set_ylabel("Rad21", fontsize=label_fontsize)
barlist = ax_e.bar(np.arange(end_e - start_e), rad21[start_e:end_e],
                   linewidth=0, color='#1f77b4')
ax_e.set_xlim([0, end_e - start_e])
#ax_e.set_ylim([np.nanmin(exclusion[start_e:end_e]),
#               np.nanmax(exclusion[start_e:end_e])])
ax_e.spines['top'].set_visible(False)
ax_e.spines['right'].set_visible(False)
#ax_e.axhline(y=np.nanpercentile(tmp, 90), linestyle='--', linewidth=1, color='black')
ax_e.set_xticks([])



# Genes
ax_g.set_ylabel('Genes', labelpad=22, fontsize=label_fontsize)
ax_g.set_xlim([start_g, end_g])
arrow_step = (end_g - start_g) / 300
spacing_g = (end_g - start_g) / 1000
txt_spacing_g = (end_g - start_g) / 1000
exon_height = 1.4
row_step = -2
font_size_g = 3
renderer_g = fig.canvas.get_renderer()
rendered_axis_width_g = ax_g.get_window_extent().width
axis_width_g = end_g - start_g
rendered_to_coord_width = axis_width_g / rendered_axis_width_g
lw_g = 0.1
color_g = 'darkred'
color_hist = 'green'

# Draw genes from right positions
most_right_pos = [] # Most left positions of each row

for gene, twidth in zip(genes, twidth_list):
    row = 0
    row_idx = -1
    for i, p in enumerate(most_right_pos):
        if p < gene.txStart - twidth - spacing_g:
            row = i * row_step
            row_idx = i
            break
    if row_idx == -1:
        row = len(most_right_pos) * row_step
        row_idx = len(most_right_pos)
        most_right_pos += [0]
        
    ax_g.plot([gene.txStart, gene.txEnd], [row, row], lw=lw_g, zorder=-10, color=color_g)

    tbox = ax_g.text(gene.txStart - txt_spacing_g, row, gene.name,
                     ha="right", va="center",
                     zorder=10, fontsize=font_size_g, clip_on=True)
    most_right_pos[row_idx] = gene.txEnd

    # Line plot is extended out through txStart and txEnd. Hiding them by Rectangle.
    rect = Rectangle((gene.txStart - txt_spacing_g, row - exon_height/4.0),
                     width=txt_spacing_g, height=exon_height/2.0, zorder=-1, fc='white', lw=0)
    ax_g.add_patch(rect)
    rect = Rectangle((gene.txEnd, row - exon_height/4.0),
                     width=txt_spacing_g, height=exon_height/2.0, zorder=-1, fc='white', lw=0)
    ax_g.add_patch(rect)

    
    if gene.strand == "+":
        x_delta = (end_g - start_g) / 1000
        y_delta = exon_height / 8.
        
        for pos in range(gene.txStart + arrow_step, gene.txEnd, arrow_step):
            ax_g.plot([pos - x_delta, pos, pos - x_delta], [row + y_delta, row, row - y_delta],
                      lw=lw_g / 100., color=color_g, zorder=0)
    else:
        x_delta = (end_g - start_g) / 700
        y_delta = exon_height / 8.

        for pos in range(gene.txEnd - arrow_step, gene.txStart, -arrow_step):
            ax_g.plot([pos + x_delta, pos, pos + x_delta], [row + y_delta, row, row - y_delta],
                      lw=lw_g / 100., color=color_g, zorder=0)

    for exon_idx in range(len(gene.exonStarts)):
        # This exon is UTR
        if gene.exonFrames[exon_idx] == -1:
            exon_rect = Rectangle((gene.exonStarts[exon_idx], row - exon_height/4.0),
                                  width=gene.exonEnds[exon_idx] - gene.exonStarts[exon_idx],
                                  height=exon_height/2.0, zorder=10, fc=color_g, lw=0)
            ax_g.add_patch(exon_rect)
        else:
            # init as no UTR
            exonStart = gene.exonStarts[exon_idx]
            exonEnd = gene.exonEnds[exon_idx]

            # UTR included at exonStart
            if gene.exonStarts[exon_idx] < gene.cdsStart and gene.cdsStart < gene.exonEnds[exon_idx]:
                utrStart_left = gene.exonStarts[exon_idx]
                utrEnd_left = gene.cdsStart
                exonStart = gene.cdsStart
                exon_rect = Rectangle((utrStart_left, row - exon_height/4.0),
                                      width=utrEnd_left - utrStart_left,
                                      height=exon_height/2.0, zorder=10, fc=color_g, lw=0)
                ax_g.add_patch(exon_rect)
                
            # UTR included at exonEnd
            if gene.exonStarts[exon_idx] < gene.cdsEnd and gene.cdsEnd < gene.exonEnds[exon_idx]:
                exonEnd = gene.cdsEnd
                utrStart_right = gene.cdsEnd
                utrEnd_right = gene.exonEnds[exon_idx]
                exon_rect = Rectangle((utrStart_right, row - exon_height/4.0),
                                      width=utrEnd_right - utrStart_right,
                                      height=exon_height/2.0, zorder=10, fc=color_g, lw=0)
                ax_g.add_patch(exon_rect)
                
            exon_rect = Rectangle((exonStart, row - exon_height/2.0),
                                  width=exonEnd - exonStart,
                                  height=exon_height, zorder=10, fc=color_g, lw=0)
            ax_g.add_patch(exon_rect)


ax_g.spines['right'].set_visible(False)
ax_g.spines['left'].set_visible(False)
ax_g.spines['top'].set_visible(False)
ax_g.spines['bottom'].set_visible(False)
ax_g.set_xticks([])
ax_g.set_yticks([])

# Save
plt.savefig(out_file, format='eps', bbox_inches="tight", pad_inches=0.2)
plt.close()
